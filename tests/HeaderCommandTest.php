<?php

namespace DanielBehrendt\WebScraper\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use DanielBehrendt\WebScraper\Commands\HeaderCommand;

class HeaderCommandTest extends TestCase
{
    public function testExecute(): void
    {
        $commandName = 'web-scraper:header';

        $application = new Application();
        $application->add(new HeaderCommand($commandName));

        $command = $application->find($commandName);

        $commandTester = new CommandTester($command);

        $commandTester->execute([
            'url' => 'https://httpbin.org/html',
            '--format' => 'json',
        ]);

        $display = $commandTester->getDisplay();

        $this->assertIsString($display);
    }
}
