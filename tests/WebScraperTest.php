<?php

namespace DanielBehrendt\WebScraper\Tests;

use DanielBehrendt\WebScraper\Scrapers\HeaderScraper;
use DanielBehrendt\WebScraper\Scrapers\ScraperInterface;
use GuzzleHttp\Psr7\Uri;
use PHPUnit\Framework\TestCase;
use ReflectionException;
use ReflectionMethod;
use Spatie\Crawler\Crawler;
use Tightenco\Collect\Support\Collection;
use DanielBehrendt\WebScraper\WebScraper;

class WebScraperTest extends TestCase
{
    public function testWebScraper(): WebScraper
    {
        $clientOptions = [
            'idn_conversion' => false,
            'allow_redirects' => true,
        ];

        $webScraper = new WebScraper($clientOptions);

        $this->assertInstanceOf(WebScraper::class, $webScraper);

        return $webScraper;
    }

    /**
     * @depends testWebScraper
     * @param WebScraper $webScraper
     * @return Crawler
     * @throws ReflectionException
     */
    public function testGetCrawler(WebScraper $webScraper): Crawler
    {
        $method = new ReflectionMethod(get_class($webScraper), 'getCrawler');
        $method->setAccessible(true);

        $crawler = $method->invoke($webScraper);

        $this->assertInstanceOf(Crawler::class, $crawler);

        return $crawler;
    }

    /**
     * @depends testWebScraper
     * @param WebScraper $webScraper
     * @return WebScraper
     * @throws ReflectionException
     */
    public function testSetScraper(WebScraper $webScraper): WebScraper
    {
        $webScraper = $webScraper->setScraper(new HeaderScraper());

        $this->assertInstanceOf(WebScraper::class, $webScraper);

        $method = new ReflectionMethod(get_class($webScraper), 'getScraper');
        $method->setAccessible(true);

        $this->assertInstanceOf(HeaderScraper::class, $method->invoke($webScraper));

        return $webScraper;
    }

    /**
     * @depends testWebScraper
     * @param WebScraper $webScraper
     * @throws ReflectionException
     */
    public function testGetScraper(WebScraper $webScraper): void
    {
        $method = new ReflectionMethod(get_class($webScraper), 'getScraper');
        $method->setAccessible(true);

        $scraper = $method->invoke($webScraper);

        $this->assertInstanceOf(ScraperInterface::class, $scraper);
    }

    /**
     * @depends testWebScraper
     * @param WebScraper $webScraper
     * @return Crawler
     * @throws ReflectionException
     */
    public function testGetCrawlerWithOptions(WebScraper $webScraper): Crawler
    {
        $crawlerOptions = [
            'maximumCrawlCount' => 3,
            'maximumDepth' => 3,
            'parseableMimeTypes' => ['application/pdf'],
        ];

        $method = new ReflectionMethod(get_class($webScraper), 'getCrawlerWithOptions');
        $method->setAccessible(true);

        /** @var Crawler $crawler */
        $crawler = $method->invokeArgs($webScraper, [$crawlerOptions]);

        $this->assertInstanceOf(Crawler::class, $crawler);
        $this->assertEquals(3, $crawler->getMaximumCrawlCount());
        $this->assertEquals(3, $crawler->getMaximumDepth());
        $this->assertTrue(in_array('application/pdf', $crawler->getParseableMimeTypes()));
        $this->assertFalse(in_array('text/html', $crawler->getParseableMimeTypes()));

        return $crawler;
    }

    /**
     * @depends testWebScraper
     * @param WebScraper $webScraper
     */
    public function testGetResults(WebScraper $webScraper): void
    {
        $results = $webScraper->getResults('https://httpbin.org/html');
        $this->assertInstanceOf(Collection::class, $results);

        $results404 = $webScraper->getResults('http://httpbin.org/status/404');
        $this->assertEquals(1, $results404->count());

        $noResults = $webScraper->getResults('httpbin.org');
        $this->assertNull($noResults);
    }

    /**
     * @depends testWebScraper
     * @param WebScraper $webScraper
     * @throws ReflectionException
     */
    public function testValidateUrl(WebScraper $webScraper): void
    {
        $method = new ReflectionMethod(get_class($webScraper), 'validateUrl');
        $method->setAccessible(true);

        $this->assertFalse($method->invokeArgs($webScraper, ['httpbin.org/status/200']));
        $this->assertInstanceOf(Uri::class, $method->invokeArgs($webScraper, ['//httpbin.org/status/200']));
        $this->assertIsString((string) $method->invokeArgs($webScraper, ['//httpbin.org/status/200']));
        $this->assertIsNotString($method->invokeArgs($webScraper, ['//httpbin.org/status/200']));
        $this->assertStringStartsWith('http', (string)$method->invokeArgs($webScraper, ['//httpbin.org/status/200']));
        $this->assertStringStartsNotWith('http://', (string)$method->invokeArgs($webScraper, ['https://httpbin.org/status/200']));
        $this->assertStringEndsWith('/', (string)$method->invokeArgs($webScraper, ['https://httpbin.org']));
    }
}
