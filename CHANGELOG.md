# Changelog
All notable changes to `danielbehrendt\web-scraper` will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.0] - 2020-04-25
### Added
- Added tests for console commands.
### Changed 
- Changed the way in which commands work. Arguments and options must be passed directly. They do not ask for input anymore.
- Switched badges from Shield.io to Badgen.

## [1.0.5] - 2020-04-24
### Fixed
- Wrong links on badges in README.

## [1.0.4] - 2020-04-24
### Added
- Enabled coverage in PHPUnit test.
- Enabled caching for vendor dir in CI scripts.
- Added missing "Unreleased" section to CHANGELOG.

## [1.0.3] - 2020-04-23
### Changed
- Switched badges in README to Shields.io.

## [1.0.2] - 2020-04-23
### Fixed
- Fixed quotation marks of markup selectors.

## [1.0.1] - 2020-04-23
### Added
- Badges in README.
- Format info in CHANGELOG.
- Semantic versioning info in CHANGELOG. 
### Fixed
- Fixed missing namespaces to "Full example" in README.

## [1.0.0] - 2020-04-23
### Added
- All files for the first release.
