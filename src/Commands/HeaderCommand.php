<?php

namespace DanielBehrendt\WebScraper\Commands;

use DanielBehrendt\WebScraper\WebScraper;
use DanielBehrendt\WebScraper\Scrapers\HeaderScraper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HeaderCommand extends BaseCommand
{
    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $url = $input->getArgument('url');
        $format = $input->getOption('format');

        $webScraper = new WebScraper();

        $results = $webScraper
            ->setScraper(new HeaderScraper())
            ->getResults($url);

        if ('json' === $format) {

            echo $results->toJson();

        } else {
            $this->dump($results);
        }

        return 0;
    }
}
