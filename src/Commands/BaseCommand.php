<?php

namespace DanielBehrendt\WebScraper\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;

abstract class BaseCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Scrape headers from a given URL')
            ->setDefinition(
                new InputDefinition([
                    new InputArgument('url', InputArgument::REQUIRED),
                    new InputOption('format', 'f', InputOption::VALUE_OPTIONAL, '', 'dump'),
                ])
            );
    }

    /**
     * @param $var
     */
    protected function dump($var): void
    {
        $cloner = new VarCloner();
        $cloner->setMaxItems(-1);

        $dumper = new CliDumper();
        $dumper->dump($cloner->cloneVar($var));
    }
}
