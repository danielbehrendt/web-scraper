<?php

namespace DanielBehrendt\WebScraper\Scrapers;

class HeaderScraper extends BaseScraper
{
    /** @var bool */
    public bool $showStatus = true;

    /** @var bool */
    public bool $showHeaders = true;

    /**
     * {@inheritdoc}
     */
    public function getElementSelectors(): array
    {
        return [];
    }
}
