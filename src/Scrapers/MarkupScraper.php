<?php

namespace DanielBehrendt\WebScraper\Scrapers;

class MarkupScraper extends BaseScraper
{
    /** @var bool */
    public bool $showStatus = false;

    /** @var bool */
    public bool $showHeaders = false;

    /**
     * {@inheritdoc}
     */
    public function getElementSelectors(): array
    {
        return [
            'html.language' => [
                'filter' => '//html/@lang|//meta[@http-equiv="content-language"]/@content',
            ],
            'html.head.title' => [
                'filter' => '//*/title',
            ],
            'html.head.meta.canonical' => [
                'filter' => '//link[@rel="canonical"]/@href',
            ],
            'html.head.meta.publisher' => [
                'filter' => '//head/link[@rel="publisher"]/@href',
            ],
            'html.head.meta.favicon' => [
                'filter' => '//link[contains(@rel, "icon")]/@href',
            ],
            'html.head.meta.description' => [
                'filter' => '//meta[@name="description"]/@content',
            ],
            'html.head.meta.keywords' => [
                'filter' => '//meta[@name="keywords"]/@content',
            ],
            'html.head.meta.robots' => [
                'filter' => '//meta[@name="robots"]/@content',
            ],
            'html.head.meta.viewport' => [
                'filter' => '//meta[@name="viewport"]/@content',
            ],
            'html.head.meta.charset' => [
                'filter' => '//html/head/meta/@charset|//html/head/meta[@http-equiv="Content-Type"]/@content',
            ],
            'html.length' => [
                'filter' => function ($html) {
                    return strlen($html);
                },
            ],
            'html.text.length' => [
                'filter' => function ($html) {
                    // get text from all tags without style and script tags
                    $results = $this->getElementsFromHtmlBySelector(
                        $html,
                        '//*[name() != "style" and name() != "script"]/text()'
                    );

                    if ($results) {
                        $results = array_filter(
                            $results,
                            function ($item) {

                                return preg_match("/\S/", $item);
                            }
                        );

                        return strlen(implode('', $results));
                    }

                    return null;
                },
            ],
            'html.body.h1' => [
                'filter' => '//h1/text()',
            ],
            'html.body.iframe' => [
                'filter' => '//iframe/@src',
            ],
            'html.body.object' => [
                'filter' => '//object[@type="application/x-shockwave-flash"]/param/@value',
            ],
            'html.body.img' => [
                'filter' => '//img/@src',
            ],
            'html.body.img.withoutAlt' => [
                'filter' => '//img[@alt=""]/@src',
            ],
            'html.body.a.internal' => [
                'filter' => '//a[contains(@href, ":host") or (starts-with(@href, "/") and not(starts-with(@href, "//")))]/@href',
            ],
            'html.body.a.external' => [
                'filter' => '//a[not(contains(@href, ":host")) and (starts-with(@href, "//") or starts-with(@href, "http"))]/@href',
            ],
            'html.body.a.external.nofollow' => [
                'filter' => '//a[not(contains(@href, ":host")) and contains(@href, "nofollow") and (starts-with(@href, "//") or starts-with(@href, "http"))]/@href',
            ],
        ];
    }
}
