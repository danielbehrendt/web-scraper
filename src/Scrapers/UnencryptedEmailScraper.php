<?php

namespace DanielBehrendt\WebScraper\Scrapers;

class UnencryptedEmailScraper extends BaseScraper
{
    /** @var bool */
    public bool $showStatus = false;

    /** @var bool */
    public bool $showHeaders = false;

    /**
     * {@inheritdoc}
     */
    public function getElementSelectors(): array
    {
        return [
            'html.email.unencrypted' => [
                'filter' => function ($html) {
                    // get text from all tags without style and script tags
                    $results = $this->getElementsFromHtmlBySelector(
                        $html,
                        '//*[name() != "style" and name() != "script"]/text()'
                    );
                    if (is_array($results) || !empty(trim($results))) {

                        $text = implode('', (array)$results);

                        $pattern = '/([a-z0-9_\.\-])+(\@|\[at\])+(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+/i';

                        preg_match_all($pattern, $text, $matches);

                        if (!empty($matches[0])) {
                            return $matches[0];
                        }
                    }

                    return null;
                },
            ],
        ];
    }
}
