<?php

namespace DanielBehrendt\WebScraper\Scrapers;

interface ScraperInterface
{
    /**
     * Returns an array containing all element selectors which should be checked by the scraper.
     *
     * @return array
     */
    function getElementSelectors(): array;
}
