<?php

namespace DanielBehrendt\WebScraper\Scrapers;

use Closure;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Spatie\Crawler\CrawlObserver;
use Symfony\Component\DomCrawler\Crawler;
use Tightenco\Collect\Support\Collection;

abstract class BaseScraper extends CrawlObserver implements ScraperInterface
{
    /** @var bool */
    public bool $showStatus = true;

    /** @var bool */
    public bool $showHeaders = true;

    /** @var Collection */
    private Collection $crawledUrlCollection;

    /**
     * BaseCrawlObserver constructor.
     */
    public function __construct()
    {
        $this->initCrawledUrlCollection();
    }

    /**
     * @return $this
     */
    public function initCrawledUrlCollection(): self
    {
        $this->crawledUrlCollection = new Collection();

        return $this;
    }

    /**
     * Called when the crawling of a given URL has succeeded.
     *
     * @param UriInterface $url
     * @param ResponseInterface $response
     * @param UriInterface|null $foundOnUrl
     */
    public function crawled(UriInterface $url, ResponseInterface $response, ?UriInterface $foundOnUrl = null)
    {
        $html = $this->getBody($response);

        $elementSelectors = $this->getElementSelectors();

        if (is_string($html)) {

            $results = [];

            foreach ($elementSelectors as $key => $elementSelector) {

                $filter = $elementSelector['filter'];

                if ($filter instanceof Closure) {

                    $value = call_user_func($filter, $html);
                } else {

                    $value = $this->getElementsFromHtmlBySelector(
                        $html,
                        str_replace(':host', $url->getHost(), $filter)
                    );
                }

                $results[$key] = $value ?? null;
            }
        }

        $result = [
            'url' => (string)$url,
        ];

        if ($this->showStatus) {
            $result['status'] = $response->getStatusCode();
        }

        if ($this->showHeaders) {
            $result['headers'] = $response->getHeaders();
        }

        if (is_array($elementSelectors) && 0 < count($elementSelectors)) {
            $result['results'] = $results ?? null;
        }

        $this->crawledUrlCollection->push($result);
    }

    /**
     * Reads the body markup from a stream.
     *
     * @param ResponseInterface $response
     * @return string
     */
    private function getBody(ResponseInterface $response): string
    {
        $bodyStream = $response->getBody();

        if ($bodyStream->isSeekable()) {
            $bodyStream->rewind();
        }

        return $bodyStream->read(1024 * 1024 * 3);
    }

    /**
     * Called when the crawling of a given URL has failed.
     *
     * @param UriInterface $url
     * @param RequestException $requestException
     * @param UriInterface|null $foundOnUrl
     */
    public function crawlFailed(UriInterface $url, RequestException $requestException, ?UriInterface $foundOnUrl = null)
    {
        $response = $requestException->getResponse() ?? null;

        $this->crawledUrlCollection->push(
            [
                'url' => (string)$url,
                'status' => (!is_null($response)) ? $response->getStatusCode() : null,
                'headers' => (!is_null($response)) ? $response->getHeaders() : null,
                'message' => $requestException->getMessage(),
            ]
        );
    }

    /**
     * Called when the crawl has ended.
     *
     * @return Collection
     */
    public function finishedCrawling(): Collection
    {
        //return $this->crawledUrlCollection->unique();
        return $this->crawledUrlCollection->unique();
    }

    /**
     * Finds all elements in a HTML string by a given XPath selector.
     *
     * @param string $html
     * @param string $elementSelector
     * @return array|string|null
     */
    protected function getElementsFromHtmlBySelector(string $html, string $elementSelector)
    {
        $domCrawler = new Crawler($html);

        $nodeList = $domCrawler->filterXPath($elementSelector);

        if (0 < $nodeList->count() && is_array($values = $nodeList->extract(['_text']))) {

            return (1 === count($values)) ? reset($values) : $values;
        }

        return null;
    }
}
