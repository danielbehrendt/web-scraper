<?php

namespace DanielBehrendt\WebScraper;

use GuzzleHttp\Psr7\Uri;
use Psr\Http\Message\UriInterface;
use Spatie\Crawler\Crawler;
use Spatie\Crawler\CrawlInternalUrls;
use Tightenco\Collect\Support\Collection;
use DanielBehrendt\WebScraper\Scrapers\HeaderScraper;
use DanielBehrendt\WebScraper\Scrapers\ScraperInterface;

class WebScraper
{
    /** @var array */
    protected array $clientOptions = [];

    /** @var Crawler|null */
    private static ?Crawler $crawler = null;

    /** @var ScraperInterface|null  */
    private ?ScraperInterface $scraper = null;

    /** @var array */
    private static array $defaultCrawlerOptions = [
        'parseableMimeTypes' => [
            'text/html', 'text/plain',
        ],
    ];

    /**
     * WebScraper constructor.
     *
     * @param array $clientOptions
     */
    public function __construct(array $clientOptions = [])
    {
        $defaultClientOptions = [
            'idn_conversion' => false,
            'allow_redirects' => true,
        ];

        $this->clientOptions = array_merge($defaultClientOptions, $clientOptions);
    }

    /**
     * Returns a crawler instance (singleton).
     *
     * @return Crawler
     */
    private function getCrawler(): Crawler
    {
        if (null === static::$crawler) {
            static::$crawler = Crawler::create($this->clientOptions);
        }

        return static::$crawler;
    }

    /**
     * @return ScraperInterface
     */
    private function getScraper(): ScraperInterface
    {
        return $this->scraper ?? new HeaderScraper();
    }

    /**
     * @param ScraperInterface $scraper
     * @return $this
     */
    public function setScraper(ScraperInterface $scraper): self
    {
        $this->scraper = $scraper;

        return $this;
    }

    /**
     * Returns a crawler instance (singleton) with given options set.
     * Default options are also applied but may be overwritten:
     *
     * @param array $options = [
     *     'userAgent' => 'my-agent',
     *     'concurrency' => 1,
     *     'maximumCrawlCount' => 5,
     *     'maximumDepth' => 5,
     *     'maximumResponseSize' => 1024 * 1024 * 3,
     *     'delayBetweenRequests' => 150,
     *     'parseableMimeTypes' => [
     *         'text/html', 'text/plain',
     *     ],
     * ]
     * @return Crawler
     *@see WebScraper::getDefaultCrawlerOptions()
     */
    private function getCrawlerWithOptions(array $options = []): Crawler
    {
        $crawler = $this->getCrawler();

        $defaultOptions = static::$defaultCrawlerOptions;

        $options = array_merge($defaultOptions, $options);

        foreach ($options as $optionKey => $optionValue) {

            $crawlerMethodName = 'set' . ucfirst($optionKey);

            if (method_exists($crawler, $crawlerMethodName)) {

                $crawler->{$crawlerMethodName}($optionValue);
            }
        }

        return $crawler;
    }

    /**
     * Validates a given URL.
     * If the input doesn't start with a scheme (or a least with //) it is treated as invalid.
     *
     * @param $url
     * @return bool|Uri
     */
    private function validateUrl($url)
    {
        $urlPattern = "/^(?:https?)?:?\/\/(?:(?:(?:[\w\.\-\+!$&'\(\)*\+,;=]|%[0-9a-f]{2})+:)*
            (?:[\w\.\-\+%!$&'\(\)*\+,;=]|%[0-9a-f]{2})+@)?(?:
            (?:[a-z0-9\-\.]|%[0-9a-f]{2})+|(?:\[(?:[0-9a-f]{0,4}:)*(?:[0-9a-f]{0,4})\]))(?::[0-9]+)?(?:[\/|\?]
            (?:[\w#!:\.\?\+\|=&@$'~*,;\/\(\)\[\]\-]|%[0-9a-f]{2})*)?$/xi";

        if (!preg_match($urlPattern, (string)$url)) {

            return false;
        }

        if (!$url instanceof UriInterface) {
            $url = new Uri($url);
        }

        if ('' === $url->getScheme()) {
            $url = $url->withScheme('http');
        }

        if ('' === $url->getPath()) {
            $url = $url->withPath('/');
        }

        return $url;
    }

    /**
     * Starts crawling and returns the results of the given observer.
     *
     * @param string $url
     * @param array $options
     * @return Collection|null
     */
    public function getResults(string $url, array $options = []): ?Collection
    {
        if ($url = $this->validateUrl($url)) {

            $crawler = $this->getCrawlerWithOptions($options);

            $scraper = $this->getScraper();
            $scraper->initCrawledUrlCollection();

            $crawlProfile = $options['crawlProfile'] ?? new CrawlInternalUrls($url);

            $ignoreRobots = $options['ignoreRobots'] ?? false;

            if ($ignoreRobots) {
                $crawler->ignoreRobots();
            }

            $crawler
                ->setCrawlObserver($scraper)
                ->setCrawlProfile($crawlProfile)
                ->startCrawling($url);

            return $scraper->finishedCrawling();
        }

        return null;
    }
}
